<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>List of Employees</center>
<table border="2" width="80%">
<tr>
<th>Librarian ID</th>
<th>Librarianname</th>
<th>Gender</th>
<th>Age</th>
<th>Address</th>
</tr>
<c:forEach var="v" items="${list}">
<tr>

<td><c:out value="${v.id}"></c:out></td>
<td><c:out value="${v.librarianname}"></c:out></td>
<td><c:out value="${v.gender}"></c:out></td>
<td><c:out value="${v.age}"></c:out></td>
<td><c:out value="${v.address}"></c:out></td>
</tr>
</c:forEach>
</table>
</body>
</html>