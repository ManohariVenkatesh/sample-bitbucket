<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>List of Books</center>
<table border="5" width="90%">
<tr>
<th>Book ID</th>
<th>Book name</th>
<th>Author Name</th>
<th>Genre</th>
<th>Amount</th>
<th>Issue</th>
<th>Return</th>
</tr>
<c:forEach var="v" items="${list}">
<tr>

<td><c:out value="${v.bookid}"></c:out></td>
<td><c:out value="${v.bookname}"></c:out></td>
<td><c:out value="${v.authorname}"></c:out></td>
<td><c:out value="${v.genre}"></c:out></td>
<td><c:out value="${v.amount}"></c:out></td>
<td><a href="issuebooks">Issue</a></td>
<td><a href="returnbooks">Return</a>
</tr>
</c:forEach>
</table>
</body>
</html>