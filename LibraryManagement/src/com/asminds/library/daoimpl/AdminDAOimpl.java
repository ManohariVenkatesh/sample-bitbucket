package com.asminds.library.daoimpl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.library.dao.AdminDAO;
import com.asminds.library.pojo.AdminPojo;
import com.asminds.library.pojo.LibrarianPojo;

public class AdminDAOimpl implements AdminDAO{

	@Override
	public boolean loginsave(AdminPojo p) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(p);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public boolean save(LibrarianPojo lp) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(lp);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public List<LibrarianPojo> viewall() {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Query qry=s.createQuery("from LibrarianPojo po");
		List l=qry.list();
		System.out.println("viewall page"+"no.of.records"+l.size());
		Iterator it=l.iterator();
		while(it.hasNext()) {
			Object o=it.next();
			LibrarianPojo p=(LibrarianPojo) o;
			System.out.println(p.getId()+" "+p.getLibrarianname()+" "+p.getGender()+" "+p.getAge()+" "+p.getAddress());
		
	}
		return l;
}

	@Override
	public boolean delete(LibrarianPojo lp) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.delete(lp);
		System.out.println("deleteDAO");
		tx.commit();
		s.close();
		fac.close();
		return true;
	}
}
