 package com.asminds.library.daoimpl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.asminds.library.dao.LibrarianDAO;
import com.asminds.library.pojo.Bookspojo;
import com.asminds.library.pojo.IssuePojo;
import com.asminds.library.pojo.LibrarianLoginPojo;
import com.asminds.library.pojo.ReturnPojo;

public class LibrarianDAOimpl implements LibrarianDAO{

	@Override
	public boolean save(LibrarianLoginPojo llp) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(llp);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public boolean booksave(Bookspojo bp) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(bp);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public List<Bookspojo> viewbooks() {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Query qry=s.createQuery("from Bookspojo bp");
		List l=qry.list();
		Iterator itr=l.iterator();
		while(itr.hasNext()) {
			Bookspojo p=(Bookspojo) itr.next();
			System.out.println(p.getBookid()+" "+p.getBookname()+" "+p.getAuthorname()+" "+p.getGenre()+" "+p.getAmount());
		}
		return l;
	}

	@Override
	public boolean issueSave(IssuePojo ip) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(ip);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public boolean returnSave(ReturnPojo rp) {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Transaction tx=s.beginTransaction();
		s.save(rp);
		tx.commit();
		s.close();
		fac.close();
		return true;
	}

	@Override
	public List<IssuePojo> viewIssueBooks() {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Query qry=s.createQuery("from IssuePojo ip");
		List l=qry.list();
		Iterator itr=l.iterator();
		while(itr.hasNext()) {
			IssuePojo p=(IssuePojo) itr.next();
			System.out.println(p.getBookid()+" "+p.getBookname()+" "+p.getAuthorname()+" "+p.getGenre()+" "+p.getAmount());
		}
		return l;
	}

	@Override
	public List<ReturnPojo> viewReturnBooks() {
		Configuration con=new Configuration();
		con.configure("hibernate.cfg.xml");
		SessionFactory fac=con.buildSessionFactory();
		Session s=fac.openSession();
		Query qry=s.createQuery("from ReturnPojo rp");
		List l=qry.list();
		Iterator itr=l.iterator();
		while(itr.hasNext()) {
			ReturnPojo p=(ReturnPojo) itr.next();
			System.out.println(p.getBookid()+" "+p.getBookname()+" "+p.getAuthorname()+" "+p.getGenre()+" "+p.getAmount());
		}
		return l;
	}

	@Override
	public List<Bookspojo> viewid(Bookspojo bp) {
		Configuration cfg=new Configuration().configure("hibernate.cfg.xml");
		SessionFactory factory=cfg.buildSessionFactory();
		Session session=factory.openSession();	
		Query qry=session.createQuery("from Bookspojo e where bookid="+bp.getBookid());
		List l=qry.list();
		System.out.println("Total no of records"+l.size());
		Iterator it=l.iterator();
		while(it.hasNext()) {		
			Bookspojo p=(Bookspojo) it.next();
			System.out.println(p.getBookid()+" "+p.getBookname()+" "+p.getAuthorname()+" "+p.getGenre()+" "+p.getAmount());

		}
	
	return l;

}
}
