 package com.asminds.library.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.asminds.library.daoimpl.AdminDAOimpl;
import com.asminds.library.daoimpl.LibrarianDAOimpl;
import com.asminds.library.pojo.AdminPojo;
import com.asminds.library.pojo.Bookspojo;
import com.asminds.library.pojo.IssuePojo;
import com.asminds.library.pojo.LibrarianLoginPojo;
import com.asminds.library.pojo.LibrarianPojo;
import com.asminds.library.pojo.ReturnPojo;

@Controller
public class AdminController {
	
	@RequestMapping("/")
	public String index() {
		System.out.println("Iam in index page");
		return "index";
	}
	
	@RequestMapping("/adminlogin")
	public String adminlogin() {
		System.out.println("Iam in adminlogin page");
		return "adminlogin";
	}
	
	@RequestMapping(value="/adminloginvalidation", method=RequestMethod.POST)
	public ModelAndView adloginvalidate(@ModelAttribute("v") AdminPojo ap) {
		System.out.println("Iam in adloginvalidate");
		String username=ap.getUsername();
		String password=ap.getPassword();
		if(username.equals("admin")&&password.equals("1234")) {
			return new ModelAndView("adminpage");
		}else
		return new ModelAndView("index");
	}
	
	@RequestMapping("/addlibrarian")
	public String addlibrarian() {
		System.out.println("Iam in adminlogin page");
		return "librariandetails";
	}
	
	@RequestMapping("/librarianvalidate")
	public String savelibrarian(@ModelAttribute("t") LibrarianPojo lp) {
		AdminDAOimpl ad=new AdminDAOimpl();
		boolean s=ad.save(lp);
		if(s==true) {
			return "addsuccess";
		}else
		
		return "addfailure";
		
	}
	@RequestMapping("/viewlibrarian")
	public ModelAndView viewall() {
		AdminDAOimpl ad=new AdminDAOimpl();
		List<LibrarianPojo> p=ad.viewall();
		System.out.println("controller viewall");
		
		return new ModelAndView("viewlibrarian", "list", p);
		
	}
	@RequestMapping("/deletelibrarian")
	public String delete() {
		System.out.println("Iam in index page");
		return "deletebyid";
	}
	@RequestMapping("/deletesuccess")
	public String deletecheck(@ModelAttribute("o")LibrarianPojo lk) {
		
		System.out.println(lk.getId());
		AdminDAOimpl imp=new AdminDAOimpl();
		boolean b=imp.delete(lk);
		System.out.println("iam in delete page");
		if(b==true) {
			return "deletesuccess";
			
		}else
		
		return "adminpage";
		
	}
	@RequestMapping("/logout")
	public String logout() {
		System.out.println("logout page");
		return "index";
	}
	@RequestMapping("/librarianlogin")
	public String librarianlogin() {
		System.out.println("Iam in adminlogin page");
		return "librarianlogin";
	}
	@RequestMapping(value="/librarianloginvalidation", method=RequestMethod.POST)
	public ModelAndView libloginvalidate(@ModelAttribute("o")LibrarianLoginPojo llp) {
		String username=llp.getUsername();
		String password=llp.getPassword();
		System.out.println("librarianloginvalidation");
		if(username.equals("books")&&password.equals("12345")) {
			return new ModelAndView("librarianpage");
		}else
		
		return new ModelAndView("index");
		
	}
	@RequestMapping("/addbooks")
	public String addbooks() {
		System.out.println("Iam in librarianlogin page");
		return "bookdetails";
	}
	
	@RequestMapping("/savebooks")
	public String savebooks(@ModelAttribute("t") Bookspojo bp) {
		LibrarianDAOimpl ld=new LibrarianDAOimpl();
		boolean s=ld.booksave(bp);
		if(s==true) {
			return "addsuccess";
		}else
		
		return "addfailure";
		
	}
	@RequestMapping("/viewbooks")
	public ModelAndView viewbooks() {
	LibrarianDAOimpl ad=new LibrarianDAOimpl();
		List<Bookspojo> p=ad.viewbooks();
		System.out.println("controller viewbooks");
		
		return new ModelAndView("viewbooks", "list", p);
	}
	/*@RequestMapping("/issuebooks")
	public String issuebooks(@ModelAttribute("t") IssuePojo ip) {
	//	System.out.println(ip.getBookname());
		LibrarianDAOimpl ld=new LibrarianDAOimpl();
		boolean s=ld.issueSave(ip);
		if(s==true) {
			return "issuebooks";
		}else
		
		return "addfailure";
		
	}*/
	@RequestMapping("/issuebooks")
	public String issuebooks(@ModelAttribute("t") Bookspojo ip) {
		System.out.println(ip.getBookid());
		
		return "librarianpage";
	//	System.out.println(ip.getBookname());
		
		/*LibrarianDAOimpl ld=new LibrarianDAOimpl();
		boolean s=ld.issueSave(ip);
		if(s==true) {
			return "issuebooks";
		}else
		
		return "addfailure";*/
		
	}
	
	@RequestMapping("/viewissuedbooks")
	public ModelAndView searchone(@ModelAttribute("t")Bookspojo e) {
		System.out.println(e.getBookid());
		int i=e.getBookid();
		LibrarianDAOimpl imp=new LibrarianDAOimpl();
		
		List l=imp.viewid(e);
		System.out.println("I am in searchoneid page");
		if(l.size()!=0) 
		return new ModelAndView("viewissuedbooks", "list", l);
		else
		return new ModelAndView("error", "i", i);
	}
	@RequestMapping("/returnbooks")
	public String returnbooks(@ModelAttribute("t") ReturnPojo rp) {
		System.out.println("Returnsave");

		LibrarianDAOimpl ld=new LibrarianDAOimpl();
		boolean s=ld.returnSave(rp);
		if(s==true) {
			return "returnbooks";
		}else
		
		return "addfailure";
		
	}
	@RequestMapping("/librarianpage")
	public String librarianpage() {
		System.out.println("librarianpage page");
		return "librarianpage";
}
	@RequestMapping("/viewreturnedbooks")
	public ModelAndView viewreturnedbooks() {
	LibrarianDAOimpl ad=new LibrarianDAOimpl();
		List<ReturnPojo> p=ad.viewReturnBooks();
		System.out.println("controller viewreturnedbooks");
		
		return new ModelAndView("viewreturnedbooks", "list", p);
	}
	/*@RequestMapping("/viewissuedbooks")
	public ModelAndView viewissuedbooks() {
	LibrarianDAOimpl ad=new LibrarianDAOimpl();
		List<IssuePojo> p=ad.viewIssueBooks();
		System.out.println("controller viewissuedbooks");
		
		return new ModelAndView("viewissuedbooks", "list", p);
	}*/
}