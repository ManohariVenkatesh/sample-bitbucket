package com.asminds.library.dao;

import java.util.List;

import com.asminds.library.pojo.AdminPojo;
import com.asminds.library.pojo.LibrarianPojo;

public interface AdminDAO {
	
	public boolean loginsave(AdminPojo p);
	public boolean save(LibrarianPojo lp);
	public List<LibrarianPojo> viewall();
	public boolean delete(LibrarianPojo lp);


}
