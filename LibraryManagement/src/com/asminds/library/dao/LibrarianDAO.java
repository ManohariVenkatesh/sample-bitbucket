package com.asminds.library.dao;

import java.util.List;

import com.asminds.library.pojo.Bookspojo;
import com.asminds.library.pojo.IssuePojo;
import com.asminds.library.pojo.LibrarianLoginPojo;
import com.asminds.library.pojo.ReturnPojo;

public interface LibrarianDAO {
	public boolean save(LibrarianLoginPojo llp);
	public boolean booksave(Bookspojo bp);
	public List<Bookspojo> viewbooks();
	public List<Bookspojo> viewid(Bookspojo bp);

	public boolean issueSave(IssuePojo ip);
	public List<IssuePojo> viewIssueBooks();

	public boolean returnSave(ReturnPojo rp);
	public List<ReturnPojo> viewReturnBooks();


}
