package com.asminds.library.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Librariandetails")
public class LibrarianPojo {
	@Id
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private int id;
	private String librarianname;
	private String gender;
	private int age;
	private String address;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibrarianname() {
		return librarianname;
	}
	public void setLibrarianname(String librarianname) {
		this.librarianname = librarianname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public LibrarianPojo(int id, String librarianname, String gender, int age, String address) {
		super();
		this.id = id;
		this.librarianname = librarianname;
		this.gender = gender;
		this.age = age;
		this.address = address;
	}
	public LibrarianPojo() {
		
	}
	

}
